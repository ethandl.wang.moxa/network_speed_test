#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>     
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>

//#define RUN_ON_CLOUD

#ifdef RUN_ON_CLOUD
#define HOST_ADDR_STR "10.0.0.4"
#define FILE_TO_CLIENT "/home/moxa/network_speed_test/linux-4.4.tar.gz"
#define FILE_FROM_CLIENT "/home/moxa/network_speed_test/tmp_file_from_client"
#else
#define HOST_ADDR_STR "10.0.20.132"
#define FILE_TO_CLIENT "/home/moxa/code/gitlab/network_speed_test/linux-4.4.tar.gz"
#define FILE_FROM_CLIENT "/home/moxa/code/gitlab/network_speed_test/tmp_file_from_client"
#endif
#define TCP_PORT 11111
#define LISTEN_BACKLOG 16
#define MAX_BUFFER_LEN (1024)
#define CMD_LINE_LEN (13)
#define SERVER2CLIENT_CMD "rx-speed-test"
#define CLIENT2SERVER_CMD "tx-speed-test"

int main(void)
{
    struct sockaddr_in server_sockaddr;
    int ret;
    in_addr_t server_addr_ip4;
    int accept_fd;
    char buff[MAX_BUFFER_LEN];
    char cmd_buff[16];
    pid_t pid;
    int reuse=1;
    int max_buff_size = 212992;
    int len = 0;
    int count = 0;
    ssize_t send_size = 0;
    ssize_t write_size = 0;
    struct timeval tv_before;
    struct timezone tz_before;
    struct timeval tv_after;
    struct timezone tz_after;
    struct stat statbuff_file_to_client;
    double use_time;
    int fd_to_client = 0;
    int fd_from_client = 0;

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0){
        perror("socket error");
        return -1;
    }
    //server ip&port
    memset(&server_sockaddr, 0, sizeof(struct sockaddr_in));
    server_sockaddr.sin_family = AF_INET;
    ret = inet_pton(AF_INET, HOST_ADDR_STR, &(server_sockaddr.sin_addr.s_addr));
    if(ret != 1){
        perror("inet_pton error");
        close(socket_fd);
        return -1;
    }
    server_sockaddr.sin_port = htons(TCP_PORT);

    //can execute app repeated in a short time
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(int));
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(int));

    ret = bind(socket_fd, (struct sockaddr *)&server_sockaddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        perror("bind error");
        close(socket_fd);
        return -1;
    }    

    ret = listen(socket_fd, LISTEN_BACKLOG);
    if(ret != 0){
        perror("listen error");
        close(socket_fd);
        return -1;
    }     

    while(1){
        accept_fd = accept(socket_fd, NULL, NULL);
        if(accept_fd < 0){
            perror("accept error");
            close(socket_fd);
            exit(-5);
        }  
        
        pid = fork();
        if(pid < 0){
            perror("fork error:");
            close(socket_fd);
            exit(-1);
        }else if(pid == 0){
            //child process
            close(socket_fd);
            
            //handle request form client
            while(1){
                memset(buff, 0, sizeof(buff));
                ret = recv(accept_fd, buff, sizeof(char)*CMD_LINE_LEN, 0);
                if(ret == 0){
                    printf("connect is disconnected\n");
                    close(accept_fd);
                    exit(-5); 
                } else if(ret < 0){
                    perror("recv error:");
                    close(accept_fd);
                    exit(-5);
                }
                printf("receive info from client:%s\n", buff);  

                if(strlen(buff) == strlen(SERVER2CLIENT_CMD) && strncmp(buff, SERVER2CLIENT_CMD, strlen(SERVER2CLIENT_CMD)) == 0){

                    fd_to_client = open(FILE_TO_CLIENT, O_RDONLY);
                    if(fd_to_client < 0){
                        perror("open error:");
                        close(accept_fd);
                        exit(-5);
                    }   
                    
                    if((ret = fstat(fd_to_client, &statbuff_file_to_client)) != 0){
                        perror("fstat error:");
                        close(fd_to_client);
                        close(accept_fd);
                        exit(-5);
                    }

                    send_size = sendfile(accept_fd, fd_to_client, NULL, (size_t)statbuff_file_to_client.st_size);
                    if(send_size != statbuff_file_to_client.st_size){
                        printf("%s %d send_size=%ld file size=%ld\n", __FUNCTION__, __LINE__, send_size, statbuff_file_to_client.st_size);
                    }

                    printf("%s %d file send finish, size: %ld Bytes\n", __FUNCTION__, __LINE__, send_size);
                    close(fd_to_client);
                    close(accept_fd);
                    exit(0);
                } else if(strlen(buff) == strlen(CLIENT2SERVER_CMD) && strncmp(buff, CLIENT2SERVER_CMD, strlen(CLIENT2SERVER_CMD)) == 0){
                    remove(FILE_FROM_CLIENT);
                    fd_from_client = open(FILE_FROM_CLIENT, O_WRONLY|O_TRUNC|O_CREAT);
                    if(fd_from_client < 0){
                        perror("open error:");
                        close(accept_fd);
                        return -1;
                    }
                    //printf("%s %d\n", __FUNCTION__, __LINE__);
                    
                    memset(buff, 0, sizeof(buff));
                    ret = gettimeofday(&tv_before, &tz_before);
                    while((ret = recv(accept_fd, buff, sizeof(buff), 0)) > 0){
                        //printf("%s %d ret=%d count=%d\n", __FUNCTION__, __LINE__, ret, ++count);
                        write_size = write(fd_from_client, buff, ret);
                        if(write_size != ret){
                            printf("there are %d bytes data but just write in file %ld bytes.\n", ret, write_size);
                        }
                        memset(buff, 0, sizeof(buff));
                    }
                    ret = gettimeofday(&tv_after, &tz_after);
                    
                    
                    ret = fstat(fd_from_client, &statbuff_file_to_client);
                    if(ret != 0){
                        perror("lstat error:");
                        close(socket_fd);
                        close(accept_fd);
                        exit(-5);
                    }
                    close(fd_from_client);
                    use_time = (double)(tv_after.tv_sec - tv_before.tv_sec)*1000000 + (tv_after.tv_usec - tv_before.tv_usec);
                    printf("download use time by gettimeofday %.1f ms\n", use_time/1000);
                    printf("download file size: %ld bytes.\n", statbuff_file_to_client.st_size);
                    printf("DownLoad BandWidth: %.1f Mbps\n", ((statbuff_file_to_client.st_size*8)/(1024*1024))/(use_time/1000000));
                    printf("==================================================================================================\n");

                    #if 1
                    sleep(1);
                    memset(buff, 0, sizeof(buff));
                    snprintf(buff, sizeof(buff), "%ld", statbuff_file_to_client.st_size);
                    send(accept_fd, buff, strlen(buff), 0);

                    sleep(1);
                    memset(buff, 0, sizeof(buff));
                    use_time = (double)(tv_after.tv_sec - tv_before.tv_sec)*1000000 + (tv_after.tv_usec - tv_before.tv_usec);
                    snprintf(buff, sizeof(buff), "%.3f", use_time/1000000);
                    send(accept_fd, buff, strlen(buff), 0);

                    sleep(1);
                    memset(buff, 0, sizeof(buff));
                    snprintf(buff, sizeof(buff), "%.3f", ((statbuff_file_to_client.st_size*8)/(1024*1024))/(use_time/1000000));
                    send(accept_fd, buff, strlen(buff), 0);
                    #endif
                    
                    close(accept_fd);
                    exit(0);
                }

            }
            close(accept_fd);
            exit(0);

        } else if(pid > 0){
            //parent process
            close(accept_fd);

        }

    }

    return 0;

}