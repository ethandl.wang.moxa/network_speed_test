CC=gcc
app_test=network_test
app_server=server
app_client=client
test_fork=test_fork
test_fstat=test_fstat

network_test:main.c
	${CC} -o ${app_test} main.c -lrt

server: server.c
	${CC} -o ${app_server} server.c 

client: client.c
	${CC} -o ${app_client} client.c 

test_fork: test_fork.c
	${CC} -o ${test_fork} test_fork.c 

test_fstat: test_fstat.c
	${CC} -o ${test_fstat} test_fstat.c 

clean:
	rm -rf ${app_test} ${app_server} ${app_client} ${test_fork} ${test_fstat}