#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define TEST_FILE "/home/moxa/code/ftp/FWR_UC-8100_V3.4_Build_20101418.img"

int main(void)
{
    int ret;
    struct stat buff;
    int fd = open(TEST_FILE, O_RDONLY);
    if(fd < 0){
        perror("fstat error:");
        return -1;
    }    
    ret = fstat(fd, &buff);
    if(ret < 0){
        perror("fstat error:");
        return -1;
    }     

    printf("%s size %ld bytes\n", TEST_FILE, buff.st_size);

    close(fd);
    return 0;
}