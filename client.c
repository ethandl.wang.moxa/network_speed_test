#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>     
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>

//#define SERVER_RUN_ON_CLOUD
//#define CLIENT_RUN_ON_ARM

#ifdef SERVER_RUN_ON_CLOUD
#define SERVER_ADDR_STR "40.73.162.155"
#else
#define SERVER_ADDR_STR "10.0.20.132"
#endif
#define SERVER_TCP_PORT 11111
#define CLIENT_TCP_PORT_DOWNLOAD 22222
#define CLIENT_TCP_PORT_UPLOAD 22223
#define MAX_BUFFER_LEN 1024

#ifdef CLIENT_RUN_ON_ARM
#define FILE_FROM_SERVER "/home/moxa/network_speed_test/tmp_file_from_server"
#define FILE_TO_SERVER "/home/moxa/network_speed_test/linux-4.4.tar.gz"
#else
#define FILE_FROM_SERVER "/home/moxa/code/gitlab/network_speed_test/tmp_file_from_server"
#define FILE_TO_SERVER "/home/moxa/code/gitlab/network_speed_test/linux-4.4.tar.gz"
#endif

#define SERVER2CLIENT_CMD "rx-speed-test"
#define CLIENT2SERVER_CMD "tx-speed-test"

struct speed_test_info {
    double RTT_time; //ms
    off_t file_size; //bytes
    double transfer_time; //s
    double band_width; //Mbps
};

int main(void)
{
    struct sockaddr_in server_sockaddr;
    struct speed_test_info download_speed_info, upload_speed_info; 
    int ret;
    int count = 0;
    in_addr_t server_addr_ip4;
    int accept_fd;
    char buff[MAX_BUFFER_LEN];
    struct timeval tv_before;
    struct timezone tz_before;
    struct timeval tv_after;
    struct timezone tz_after;
    int reuse=1;
    int len = 0;
    ssize_t send_size = 0;
    ssize_t recv_size = 0;
    ssize_t write_size = 0;
    double use_time;
    struct stat statbuff_file_from_server;
    int socket_fd;

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0){
        perror("socket error");
        return -1;
    }

    memset(&server_sockaddr, 0, sizeof(struct sockaddr_in));
    server_sockaddr.sin_family = AF_INET;
    server_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_sockaddr.sin_port = htons(CLIENT_TCP_PORT_DOWNLOAD);

    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(int));
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(int));

    ret = bind(socket_fd, (struct sockaddr *)&server_sockaddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        perror("bind error");
        close(socket_fd);
        return -1;
    }    

    memset(&server_sockaddr, 0, sizeof(struct sockaddr_in));
    server_sockaddr.sin_family = AF_INET;
    ret = inet_pton(AF_INET, SERVER_ADDR_STR, &(server_sockaddr.sin_addr.s_addr));
    if(ret != 1){
        perror("inet_pton error");
        close(socket_fd);
        return -1;
    }
    server_sockaddr.sin_port = htons(SERVER_TCP_PORT);

    ret = gettimeofday(&tv_before, &tz_before);
    ret = connect(socket_fd, (struct sockaddr *)&server_sockaddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        perror("connect error");
        close(socket_fd);
        return -1;
    }
    ret = gettimeofday(&tv_after, &tz_after);
    use_time = (double)(tv_after.tv_sec - tv_before.tv_sec)*1000000 + (tv_after.tv_usec - tv_before.tv_usec);
    //printf("connect use time by gettimeofday %.3f ms\n", use_time/1000);
    download_speed_info.RTT_time = use_time/1000;

    //download
    memset(buff, 0, sizeof(buff));
    snprintf(buff, sizeof(buff), "%s", SERVER2CLIENT_CMD);
    send(socket_fd, buff, strlen(buff), 0);

    remove(FILE_FROM_SERVER);
    int download_fd = open(FILE_FROM_SERVER, O_WRONLY|O_TRUNC|O_CREAT);
    if(download_fd < 0){
        perror("open error:");
        close(socket_fd);
        return -1;
    }
    memset(buff, 0, sizeof(buff));
    ret = gettimeofday(&tv_before, &tz_before);
    while((recv_size = recv(socket_fd, buff, sizeof(buff), 0)) > 0){
        //printf("%s %d ret=%d count=%d\n", __FUNCTION__, __LINE__, ret, ++count);
        write_size = write(download_fd, buff, recv_size);
        if(write_size != recv_size){
            printf("%s %d write_size=%ld recv_size=%ld\n", __FUNCTION__, __LINE__, write_size, recv_size);
        }
        //printf("%s %d write_size=%ld ret=%d count=%d\n", __FUNCTION__, __LINE__, write_size, ret, ++count);
        memset(buff, 0, sizeof(buff));
    }
    ret = gettimeofday(&tv_after, &tz_after);
    use_time = (double)(tv_after.tv_sec - tv_before.tv_sec)*1000000 + (tv_after.tv_usec - tv_before.tv_usec);
    //printf("download use time by gettimeofday %.1f ms\n", use_time/1000);
    download_speed_info.transfer_time = use_time/1000000;

    
    ret = fstat(download_fd, &statbuff_file_from_server);
    if(ret != 0){
        perror("lstat error:");
        close(socket_fd);
        close(download_fd);
        return -1;
    }
    close(download_fd);
    close(socket_fd);

    //printf("download file size: %ld bytes.\n", download_file_info.st_size);
    //printf("DownLoad BandWidth: %.1f Mbps\n", ((download_file_info.st_size*8)/(1024*1024))/(use_time/1000000));
    //printf("==================================================================================================\n");
    download_speed_info.file_size = statbuff_file_from_server.st_size;
    download_speed_info.band_width = ((download_speed_info.file_size*8)/(1024*1024))/download_speed_info.transfer_time;

    //upload
    sleep(1);
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0){
        perror("socket error");
        return -1;
    }

    memset(&server_sockaddr, 0, sizeof(struct sockaddr_in));
    server_sockaddr.sin_family = AF_INET;
    server_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_sockaddr.sin_port = htons(CLIENT_TCP_PORT_UPLOAD);
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(int));
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(int));

    ret = bind(socket_fd, (struct sockaddr *)&server_sockaddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        perror("bind error");
        close(socket_fd);
        return -1;
    }    

    memset(&server_sockaddr, 0, sizeof(struct sockaddr_in));
    server_sockaddr.sin_family = AF_INET;
    ret = inet_pton(AF_INET, SERVER_ADDR_STR, &(server_sockaddr.sin_addr.s_addr));
    if(ret != 1){
        perror("inet_pton error");
        close(socket_fd);
        return -1;
    }
    server_sockaddr.sin_port = htons(SERVER_TCP_PORT);

    ret = gettimeofday(&tv_before, &tz_before);
    ret = connect(socket_fd, (struct sockaddr *)&server_sockaddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        perror("connect error");
        close(socket_fd);
        return -1;
    }
    ret = gettimeofday(&tv_after, &tz_after);
    use_time = (double)(tv_after.tv_sec - tv_before.tv_sec)*1000000 + (tv_after.tv_usec - tv_before.tv_usec);
    //printf("connect use time by gettimeofday %.3f ms\n", use_time/1000);
    upload_speed_info.RTT_time = use_time/1000;

    memset(buff, 0, sizeof(buff));
    snprintf(buff, sizeof(buff), "%s", CLIENT2SERVER_CMD);
    send(socket_fd, buff, strlen(buff), 0);

    int fd_to_server = open(FILE_TO_SERVER, O_RDONLY);
    if(fd_to_server < 0){
        perror("open error:");
        close(socket_fd);
        exit(-5);
    }   
    
    if((ret = fstat(fd_to_server, &statbuff_file_from_server)) != 0){
        perror("fstat error:");
        close(fd_to_server);
        close(socket_fd);
        exit(-5);
    }

    send_size = sendfile(socket_fd, fd_to_server, NULL, (size_t)statbuff_file_from_server.st_size);
    if(send_size != statbuff_file_from_server.st_size){
        printf("%s %d send_size=%ld file size=%ld\n", __FUNCTION__, __LINE__, send_size, statbuff_file_from_server.st_size);
    }

    printf("%s %d file send finish, size: %ld Bytes\n", __FUNCTION__, __LINE__, send_size);

    shutdown(socket_fd, SHUT_WR);
    memset(buff, 0, sizeof(buff));
    if((ret = recv(socket_fd, buff, sizeof(buff), 0)) > 0){
        upload_speed_info.file_size = atoll(buff);
        //sscanf(buff, "%ld", upload_speed_info.file_size);
        //printf("receive info from server:%s\n", buff);
    }
    memset(buff, 0, sizeof(buff));
    if((ret = recv(socket_fd, buff, sizeof(buff), 0)) > 0){
        upload_speed_info.transfer_time = atof(buff);
        //sscanf(buff, "%.3f", upload_speed_info.transfer_time);
        //printf("receive info from server:%s\n", buff);
    }
    memset(buff, 0, sizeof(buff));
    if((ret = recv(socket_fd, buff, sizeof(buff), 0)) > 0){
        upload_speed_info.band_width = atof(buff);
        //sscanf(buff, "%.3f", upload_speed_info.band_width);
        //printf("receive info from server:%s\n", buff);
    }
    
    printf("-----------------------------------------------------\n");
    printf("|%14s | %-10s | %-11s | %-6s |\n", "Details", "DownLoad", "UpLoad", "Units");
    printf("-----------------------------------------------------\n");
    printf("|%14s | %-10.3f | %-11.3f | %-6s |\n", "RTT",  download_speed_info.RTT_time, upload_speed_info.RTT_time, "ms");
    printf("-----------------------------------------------------\n");
    printf("|%14s | %-10ld | %-11ld | %-6s |\n","File Size", download_speed_info.file_size, upload_speed_info.file_size, "bytes");
    printf("-----------------------------------------------------\n");
    printf("|%14s | %-10.3f | %-11.3f | %-6s |\n", "Transfer Time", download_speed_info.transfer_time, upload_speed_info.transfer_time, "s");
    printf("-----------------------------------------------------\n");
    printf("|%14s | %-10.3f | %-11.3f | %-6s |\n", "Band Width", download_speed_info.band_width, upload_speed_info.band_width, "Mbps");
    printf("-----------------------------------------------------\n");

    close(socket_fd);
    return 0;

}


