#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(void){

    int i = 3;
    pid_t pid;
    while(i--){
        sleep(1);
        pid = fork();
        if(pid < 0){
            perror("fork error:");
            exit(0);
        } else if(pid == 0){
            printf("this is child process, i=%d\n",i);
            exit(0);
        } else if(pid > 0){
            printf("this is parent process, i=%d\n",i);
        }
    }
    printf("parent process end\n");
    exit(0);
}