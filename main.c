#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{
    int ret;
    struct timespec time_info_before;
    struct timespec time_info_after;
    long temp;
    int i = 10000;

    ret = clock_gettime(CLOCK_REALTIME, &time_info_before);
    if(ret != 0){
        printf("clock_gettime fail %s %d\n", __FUNCTION__, __LINE__);
        return -1;
    }
    usleep(2);

    ret = clock_gettime(CLOCK_REALTIME, &time_info_after);
    if(ret != 0){
        printf("clock_gettime fail %s %d\n", __FUNCTION__, __LINE__);
        return -1;
    }
    printf("clock_gettime %ld, %ld\n", time_info_after.tv_sec - time_info_before.tv_sec, time_info_after.tv_nsec - time_info_before.tv_nsec);


    //int gettimeofday(struct timeval *tv, struct timezone *tz);
    struct timeval tv_before;
    struct timezone tz_before;
    struct timeval tv_after;
    struct timezone tz_after;
    ret = gettimeofday(&tv_before, &tz_before);
    //usleep(100);
    ret = gettimeofday(&tv_after, &tz_after);
    printf("gettimeofday tv %ld %ld\n", tv_after.tv_sec - tv_before.tv_sec, tv_after.tv_usec - tv_before.tv_usec);
    //printf("gettimeofday tv %ld %ld\n", tv_after.);


    

    return 0;
}